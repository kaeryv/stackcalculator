#!/usr/bin/env python3

import cmd, sys, string
from collections import deque
from termcolor import colored

class Interpreter(object):
    stack = deque()
    def __init__(self):
        self.known_commands = [func[len('cmd_')::] for func in dir(Interpreter) if callable(getattr(Interpreter, func)) and func.startswith("cmd_")]
        self.user_commands = dict()
        self.skipping = False

    def push_cmd(self, cmds):
        
        for index, cmd in enumerate(cmds):
            if not self.skipping or cmd =="then":
                if cmd.isnumeric():
                    self.stack.append(float(cmd))
                elif cmd in self.known_commands:
                    self.stack.append(str(cmd))
                    try:
                        self.bake()
                    except:
                        print("The token " + cmd + " was consumed but not executed properly.")
                elif cmd in self.user_commands:
                    self.push_cmd(self.user_commands[cmd])
                else:
                    print("Unknow token", cmd)
            else:
                pass
        pass
    
    def push_definition(self, definition):
        self.user_commands[definition[0]] = definition[1::]

    def bake(self):
        getattr(self, 'cmd_'+ self.stack.pop())()

    def cmd_add(self):
        self.stack[-1] = self.stack[-2] + self.stack.pop()
    
    def cmd_rem(self):
        self.stack[-1] = self.stack[-2] - self.stack.pop()

    def cmd_mul(self):
        self.stack[-1] = self.stack[-2] * self.stack.pop()
    
    def cmd_div(self):
        self.stack[-1] = self.stack[-2] / self.stack.pop()

    def cmd_swap(self):
        self.stack[-1], self.stack[-2] = self.stack[-2], self.stack[-1]

    def cmd_dup(self):
        self.stack.append(self.stack[-1])

    def cmd_rot(self):
        self.stack.rotate(-1)
    
    def cmd_irot(self):
        self.stack.rotate(1)

    def cmd_over(self):
        self.stack.append(self.stack[-1])

    def cmd_tuck(self):
        self.push_cmd(["swap", "over"])

    def cmd_drop(self):
        self.stack.pop()
    
    def cmd_nip(self):
        self.push_cmd(["swap", "drop"])

    def cmd_eq(self):
        self.stack[-1] = int(self.stack[-2] == self.stack.pop())

    def cmd_if(self):
        if self.stack.pop() == 1:
            pass
        else:
            self.skipping = True

    def cmd_then(self):
        if self.skipping:
            self.skipping = False

    def cmd_nop(self):
        pass
    
interp = Interpreter()

class Shell(cmd.Cmd):
    
    
    intro = "Calc 2000 v 1.0 Novembre 2019"
    prompt = colored("(calc) ", "red")

    file = None

    def do_push(self, arg):
        interp.push_cmd(arg.split(' '))
        print(colored([i for i in interp.stack],'yellow'))

    def do_print(self, arg):
        print(colored([i for i in interp.stack],'yellow'))

    def do_clear(self, arg):
        print(colored("Cleared stack", 'cyan'))
        interp.stack.clear()

    def do_def(self, arg):
        interp.push_definition(arg.split(' '))

    def do_loop(self, arg):
        program = arg.split(' ')
        print(colored("Looping routine " + str(program[0]) + " times.", 'cyan'))
        for i in range(int(program[0])):
            interp.push_cmd(program[1::])

    def do_exec(self, arg):
        with open(arg, 'r') as f:
            self.cmdqueue.extend([ line[1::] if line.startswith("@") else "def " + line[1::] if line.startswith("/") else "loop " + line[1::].split(":")[0] + " " + line[1::].split(":")[1] if line.startswith("$") else "push " + line for line in f.read().splitlines() if not line.startswith("#")])

if __name__ == "__main__":
    Shell().cmdloop()